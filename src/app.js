import { useState } from "react";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import Web3 from "web3";
import classes from "./components/Login/Login.module.css";
import classes2 from "./style/style.css"

const CRZTokenSale = require('./abis/CRZTokenSale.json')

const web3 = new Web3(window.web3.currentProvider);

function App() {
  const [isConnected, setIsConnected] = useState(false);
  const [currentAccount, setCurrentAccount] = useState(null);
  const [balance, setBalance] = useState(0);
  const [balanceToken, setBalanceToken] = useState(0);
  const [ValueCRZ, setValueCRZ] = useState('')

  let contract = new web3.eth.Contract(
    CRZTokenSale.abi,
    '0x5E86364d3E7262844A95FDccdEa47dC76236b5e6'
  )


  const onLogin = async (provider) => {
    const accounts = await web3.eth.getAccounts();
    if (accounts.length === 0) {
      console.log("Please connect to MetaMask!");
    } else if (accounts[0] !== currentAccount) {
      setCurrentAccount(accounts[0]);
      const accBalanceEth = web3.utils.fromWei(
        await web3.eth.getBalance(accounts[0]),
        "ether"
      );  
      setBalance(Number(accBalanceEth).toFixed(6));
      setIsConnected(true);
    }
  };

  const stake = async () =>{
    const accounts = await web3.eth.getAccounts();
    const account = accounts[0]
    contract.methods.buyTokens(ValueCRZ).send({from: account  , value:20000})
  }

  const getTokens = async() =>{
    const accounts = await web3.eth.getAccounts();
    if(contract === undefined){alert("You have 0 CRZ tokens. You can to buy"); return 0}
    contract.methods.getTokens(accounts[0]).call((err,val) => {
      setBalanceToken(val);
      alert(`You have ${val} CRZ tokens on 5 days`)
    })
  }

  const sellTokens = async() =>{
    const accounts = await web3.eth.getAccounts();
    contract.methods.getTokens(accounts[0]).call((err,val) => {
      setBalanceToken(val);
    })

    console.log(balanceToken)
    console.log(accounts[0])
    contract.methods.sellTokens(balanceToken-1).call()
  }
  

  const onLogout = () => {
    setIsConnected(false);
  };

  return (
    <div>
      <header className="main-header">
        <h1>React &amp; Web3 || 2CRZ</h1>
      </header>
      <main className="container">
        {!isConnected && <Login onLogin={onLogin} onLogout={onLogout} />}
        {isConnected && (
          <>  
          <Home currentAccount={currentAccount} balance={balance} balanceToken = {balanceToken} />
          <div style={{marginTop:'30px'}}>
          <div>How many nft tokens (CRZ) do you want to buy:</div>
          <input style={{marginTop:'20px'}}
          onChange={evt => setValueCRZ(evt.target.value)}
          ></input>
          </div>
            <div>
            <button onClick={stake} className={classes.button} style={{margin:'2rem auto'}}>Stake</button>
            <button onClick={getTokens} className={classes.button} style={{margin:'2rem auto'}}>My CRZ tokens</button>
            <button onClick={sellTokens} className={classes.button} style={{margin:'2rem auto'}}> Sell All your CRZ tokens</button>
          </div>
          </>
        )}
      </main>
    </div>
  );
}

export default App;