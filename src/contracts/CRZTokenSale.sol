// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

import "./CRZToken.sol";

contract CRZTokenSale {

    enum Functions { ERC, POOL}
    address payable public _admin;
    CRZToken public _token;
    uint256 public tokensPerEth = 100;
    uint256 private constant _TIMELOCK = 1 minutes;

    mapping(address => uint256) public _dataAdress;
    mapping(Functions  => uint) public timelock;

    event BuyTokens(address buyer, uint256 amountOfETH, uint256 amountOfTokens);

    constructor(CRZToken token) {
        _admin = msg.sender;
        _token = token;
    }

    modifier notLocked(address locker){
        require(_dataAdress[locker] <= block.timestamp, "Function is timelocked");
        _;
    }
    
    function getTokens(address _from) public view  returns(uint256){
      return _token.getTokens(_from);
    }

    function lockAddress(address locker) public {
        _dataAdress[locker] = block.timestamp + _TIMELOCK;
    }

    function unlockAddress(address locker) public {
        _dataAdress[locker] = 0;
    }

    function buyTokens(uint256 _amount) public payable {
        require(msg.value > 0, "You need to send some ether");

        uint procent = _amount / 10;
        _amount = _amount - procent;
        getProcent();
        _token.transfer(msg.sender, _amount);
        lockAddress(msg.sender);
        emit BuyTokens(msg.sender, msg.value, _amount);
    }

    function getProcent() private{
        _admin.transfer(msg.value/10);
    }

    function sellTokens(uint256 _amount) public payable{
        _token.transferAdmin(msg.sender,_amount);
        msg.sender.transfer(_amount-1);
        unlockAddress(msg.sender);
    }

    

}