// SPDX-License-Identifier: MIT
pragma solidity ^0.7.2;

contract CRZToken {

  string public name = "CRZ Token";
  string public symbol = "CRZ";
  uint256 public decimals = 6;
  uint256 public totalSupply;
  address private _admin;

  mapping(address => uint256) public balanceOf;
  mapping(address => mapping(address => uint256)) public allowance;

  constructor() {
    _admin = msg.sender;
    totalSupply = 100000 * (10 ** decimals);
    balanceOf[msg.sender] = totalSupply;
  }

    event Transfer(
      address indexed _from,
      address indexed _to,
      uint256 _value
    );

    event Approval(
      address indexed _owner,
      address indexed _spender,
      uint256 _value
    );

  function transfer(address _to, uint256 _value) public payable returns (bool success) {

    balanceOf[_admin] -= _value;
    balanceOf[_to] += _value;

    emit Transfer(msg.sender, _to, _value);

    return true;
  }

 function getTokens(address _from) public view returns (uint256){
    return balanceOf[_from];
  }

  function transferAdmin(address _to, uint256 _value) public payable returns (bool succes){
      require(balanceOf[_to] > _value, "You need more tokens!");
      balanceOf[_admin] += _value;
      balanceOf[_to] -= _value;

      emit Transfer(msg.sender, _to, _value);

      return true;
  }

  function approve(address _spender, uint256 _value) public returns (bool success) {
    allowance[msg.sender][_spender] = _value;

    emit Approval(msg.sender, _spender, _value);

    return true;
  } 

  function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
    require(_value <= balanceOf[_from]);
    require(_value <= allowance[_from][msg.sender]);

    balanceOf[_from] -= _value;
    balanceOf[_to] += _value;

    allowance[_from][msg.sender] -= _value;

    emit Transfer(_from, _to, _value);

    return true;
  }
}