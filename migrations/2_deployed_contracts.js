const CRZToken = artifacts.require("CRZToken.sol");
const CRZTokenSale = artifacts.require("CRZTokenSale.sol");

module.exports = function(deployer) {
 deployer.deploy(CRZToken).then(function(){
   return deployer.deploy(CRZTokenSale, CRZToken.address)
 })
};